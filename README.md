# My banking example app

This is my solution for the assignment. Maybe there are some "YAGNI" features that would not be needed
for the solution. But I wanted to show some things that are important for me when I write an REST app.

## Anti Corruption Layer

The internal domain class 'Transaction' is not used directly for the REST API. This internal domain class
is mapped to an API object 'TransactionAO' which is used for the REST API mapping. Doing it like that,
you can use Spring HATEOAS for the API domain but you don't have to in the business domain.
 
## Use Money instead of Double or BigDecimal
 
When you start writing code that should calculate with 'money' it is better to use an extra money object.
This keeps your code cleaner and if you calculate with numbers you will know what is just a number and what
is a money amount. 

## Testing

I think its a good idea the write simple tests for every layer of your app. If your tests getting 
to complicated, you have to refactor your services. 

## What is missing

- REST API validation
- Domain validation when storing Transactions
- Error handling and mapping to REST API results

## O(1)

Java TreeSet (SortedSet) is used to implement a 'timestamp' index which is used for calculating statistics.
SortedSet.subset() seems to have a complexity of O(1) (Google research) to create a view 
to objects between some lower or upper bound in the set. 

For statistics calculation (sum, avg, max, min) the complexity is O(n) 
where n is the count of objects in the subset of transactions.
