package com.bank.mybanking.statistics.service;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.joda.money.CurrencyUnit.EUR;
import static org.mockito.Mockito.when;

import com.bank.mybanking.platform.time.TransactionClock;
import com.bank.mybanking.statistics.domain.Statistics;
import com.bank.mybanking.transaction.domain.Transaction;
import com.bank.mybanking.transaction.service.TransactionService;
import java.time.Instant;
import java.util.List;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceTest {

  private static final Instant NOW = Instant.now();

  private static final String ID_ONE = "111";
  private static final Money MONEY_ONE = Money.of(EUR, 1.00d);
  private static final Instant TIMESTAMP_ONE = NOW.plusSeconds(1);

  private static final String ID_TWO = "222";
  private static final Money MONEY_TWO = Money.of(EUR, 2.00d);
  private static final Instant TIMESTAMP_TWO = NOW.plusSeconds(2);

  private static final String ID_THREE = "333";
  private static final Money MONEY_THREE = Money.of(EUR, 3.00d);
  private static final Instant TIMESTAMP_THREE = NOW.plusSeconds(3);

  private static final String ID_FOUR = "444";
  private static final Money MONEY_FOUR = Money.of(EUR, 4.00d);
  private static final Instant TIMESTAMP_FOUR = NOW.plusSeconds(4);

  private static final String ID_FIVE = "555";
  private static final Money MONEY_FIVE = Money.of(EUR, 5.00d);
  private static final Instant TIMESTAMP_FIVE = NOW.plusSeconds(5);

  @Mock
  private TransactionClock transactionClock;
  @Mock
  private TransactionService transactionService;
  @InjectMocks
  private StatisticsService statisticsService;

  private Transaction transactionOne = Transaction.builder()
      .id(ID_ONE)
      .amount(MONEY_ONE)
      .timestamp(TIMESTAMP_ONE)
      .build();

  private Transaction transactionTwo = Transaction.builder()
      .id(ID_TWO)
      .amount(MONEY_TWO)
      .timestamp(TIMESTAMP_TWO)
      .build();

  private Transaction transactionThree = Transaction.builder()
      .id(ID_THREE)
      .amount(MONEY_THREE)
      .timestamp(TIMESTAMP_THREE)
      .build();

  private Transaction transactionFour = Transaction.builder()
      .id(ID_FOUR)
      .amount(MONEY_FOUR)
      .timestamp(TIMESTAMP_FOUR)
      .build();

  private Transaction transactionFive = Transaction.builder()
      .id(ID_FIVE)
      .amount(MONEY_FIVE)
      .timestamp(TIMESTAMP_FIVE)
      .build();

  private List<Transaction> transactions = asList(
      transactionOne,
      transactionTwo,
      transactionThree,
      transactionFour,
      transactionFive
  );

  @Test
  public void getTransactionStatistics() {
    when(transactionClock.now()).thenReturn(NOW);
    when(transactionService.findByTimestamp(NOW.minusSeconds(60), NOW)).thenReturn(transactions);

    final Statistics statistics = statisticsService.getTransactionStatistics().get();

    assertThat(statistics.getSum(), is(15d));
    assertThat(statistics.getAvg(), is(3d));
    assertThat(statistics.getMax(), is(5d));
    assertThat(statistics.getMin(), is(1d));
    assertThat(statistics.getCount(), is(5L));

  }
}