package com.bank.mybanking.statistics.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bank.mybanking.statistics.domain.Statistics;
import com.bank.mybanking.statistics.service.StatisticsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(StatisticsController.class)
public class StatisticsControllerTest {

  @Autowired
  private MockMvc mvc;
  @Autowired
  private ObjectMapper mapper;
  @MockBean
  private StatisticsService statisticsService;

  private Statistics statistics = new Statistics(1d, 2d, 3d, 4d, 5L);

  @Test
  @SneakyThrows
  public void getStatistics() {
    when(statisticsService.getTransactionStatistics()).thenReturn(Optional.of(statistics));

    final String json = this.mvc
        .perform(get("/statistics").accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();

    assertThat(mapper.readValue(json, Statistics.class), is(statistics));

    verify(statisticsService).getTransactionStatistics();
  }

  @Test
  @SneakyThrows
  public void getStatistics_With_Empty_Statistics() {
    when(statisticsService.getTransactionStatistics()).thenReturn(Optional.empty());

    this.mvc
        .perform(get("/statistics").accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isNotFound());

    verify(statisticsService).getTransactionStatistics();
  }

}