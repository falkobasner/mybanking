package com.bank.mybanking.platform.mapping;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JsonTest
public class JavaInstantModuleTest {

  @NoArgsConstructor
  @AllArgsConstructor
  @Data
  private static class WithInstant {

    private Instant timestamp;
  }

  private static final Instant NOW = Instant.now();
  private static final Long NOW_MILLIS = NOW.toEpochMilli();
  private static final String JSON = format("{\"timestamp\":%s}", NOW_MILLIS);

  @Autowired
  private ObjectMapper mapper;

  @Test
  @SneakyThrows
  public void serialize_To_Json() {
    assertThat(mapper.writeValueAsString(new WithInstant(NOW)), is(JSON));
  }

  @Test
  @SneakyThrows
  public void deserialize_From_Json() {
    assertThat(mapper.readValue(JSON, WithInstant.class), is(new WithInstant(NOW)));
  }

}