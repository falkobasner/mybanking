package com.bank.mybanking.platform.mapping;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JsonTest
public class JodaMoneyModuleTest {

  @NoArgsConstructor
  @AllArgsConstructor
  @Data
  private static class WithMoney {

    private Money money;
  }

  private static final Money ONE_NINE_NINE_EUR = Money.of(CurrencyUnit.EUR, 1.99d);
  private static final Money TWO_NINE_NINE_USD = Money.of(CurrencyUnit.USD, 2.99d);

  @Autowired
  private ObjectMapper mapper;

  @Test
  @SneakyThrows
  public void serialize_To_Json() {
    assertThat(mapper.writeValueAsString(new WithMoney(ONE_NINE_NINE_EUR)),
        is("{\"money\":1.99}"));
    assertThat(mapper.writeValueAsString(new WithMoney(TWO_NINE_NINE_USD)),
        is("{\"money\":\"USD 2.99\"}"));
  }

  @Test
  @SneakyThrows
  public void deserialize_From_Json() {
    assertThat(mapper.readValue("{\"money\":1.99}", WithMoney.class),
        is(new WithMoney(ONE_NINE_NINE_EUR)));
    assertThat(mapper.readValue("{\"money\":\"USD 2.99\"}", WithMoney.class),
        is(new WithMoney(TWO_NINE_NINE_USD)));
  }

}