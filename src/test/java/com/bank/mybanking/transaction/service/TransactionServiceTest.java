package com.bank.mybanking.transaction.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.joda.money.CurrencyUnit.EUR;

import com.bank.mybanking.transaction.domain.Transaction;
import java.time.Instant;
import org.joda.money.Money;
import org.junit.Test;

public class TransactionServiceTest {

  private static final Instant NOW = Instant.now();

  private static final String ID_ONE = "111";
  private static final Money MONEY_ONE = Money.of(EUR, 1.00d);
  private static final Instant TIMESTAMP_ONE = NOW.minusSeconds(61);

  private static final String ID_TWO = "222";
  private static final Money MONEY_TWO = Money.of(EUR, 2.00d);
  private static final Instant TIMESTAMP_TWO = NOW.minusSeconds(30);
  ;

  private static final String ID_THREE = "333";
  private static final Money MONEY_THREE = Money.of(EUR, 3.00d);
  private static final Instant TIMESTAMP_THREE = NOW;

  private static final String ID_FOUR = "444";
  private static final Money MONEY_FOUR = Money.of(EUR, 4.00d);

  private static final String ID_FIVE = "555";
  private static final Money MONEY_FIVE = Money.of(EUR, 5.00d);
  private static final Instant TIMESTAMP_FIVE = NOW.plusSeconds(1);

  private Transaction transactionOne = Transaction.builder()
      .id(ID_ONE)
      .amount(MONEY_ONE)
      .timestamp(TIMESTAMP_ONE)
      .build();

  private Transaction transactionTwo = Transaction.builder()
      .id(ID_TWO)
      .amount(MONEY_TWO)
      .timestamp(TIMESTAMP_TWO)
      .build();

  private Transaction transactionThree = Transaction.builder()
      .id(ID_THREE)
      .amount(MONEY_THREE)
      .timestamp(TIMESTAMP_THREE)
      .build();

  private Transaction transactionFour = Transaction.builder()
      .id(ID_FOUR)
      .amount(MONEY_FOUR)
      .timestamp(TIMESTAMP_THREE)
      .build();

  private Transaction transactionFive = Transaction.builder()
      .id(ID_FIVE)
      .amount(MONEY_FIVE)
      .timestamp(TIMESTAMP_FIVE)
      .build();

  private TransactionService transactionService = new TransactionService();

  @Test
  public void findByTimestamp() {
    transactionService.save(transactionOne);
    transactionService.save(transactionTwo);
    transactionService.save(transactionThree);
    transactionService.save(transactionFour);
    transactionService.save(transactionFive);

    // DOES NOT WORK!
    //assertThat(transactionService.findByTimestamp(transactionOne.getTimestamp(), transactionOne.getTimestamp()),
    //    contains(transactionOne));

    assertThat(transactionService.findByTimestamp(
        transactionTwo.getTimestamp(), transactionFour.getTimestamp()),
        contains(transactionTwo, transactionThree, transactionFour));

    assertThat(transactionService.findByTimestamp(
        transactionOne.getTimestamp(), transactionFive.getTimestamp()),
        contains(transactionOne, transactionTwo, transactionThree, transactionFour, transactionFive));

    assertThat(transactionService.findByTimestamp(
        transactionOne.getTimestamp().minusSeconds(60), transactionFive.getTimestamp()),
        contains(transactionOne, transactionTwo, transactionThree, transactionFour, transactionFive));

    assertThat(transactionService.findByTimestamp(
        transactionOne.getTimestamp(), transactionFive.getTimestamp().plusSeconds(60)),
        contains(transactionOne, transactionTwo, transactionThree, transactionFour, transactionFive));

    assertThat(transactionService.findByTimestamp(
        transactionOne.getTimestamp().minusSeconds(60), transactionFive.getTimestamp().plusSeconds(60)),
        contains(transactionOne, transactionTwo, transactionThree, transactionFour, transactionFive));

    assertThat(transactionService.findByTimestamp(
        transactionFive.getTimestamp(), transactionFive.getTimestamp().plusSeconds(1)),
        contains(transactionFive));
  }
}