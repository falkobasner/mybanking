package com.bank.mybanking.transaction.api.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.joda.money.CurrencyUnit.EUR;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import com.bank.mybanking.platform.time.TransactionClock;
import com.bank.mybanking.transaction.api.domain.CreateTransactionResult;
import com.bank.mybanking.transaction.api.domain.CreateTransactionResult.Status;
import com.bank.mybanking.transaction.api.domain.TransactionAO;
import com.bank.mybanking.transaction.api.domain.TransactionAOMapper;
import com.bank.mybanking.transaction.domain.Transaction;
import com.bank.mybanking.transaction.service.TransactionService;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import org.joda.money.Money;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.hateoas.Link;

@RunWith(MockitoJUnitRunner.class)
public class TransactionApiServiceTest {

  private static final String ID = "111";
  private static final Money MONEY = Money.of(EUR, 1.99d);
  private static final Instant TIMESTAMP = Instant.now();
  private static final String HREF = "http://bank.com/transactions/" + ID;

  @Mock
  private TransactionClock transactionClock;
  @Mock
  private TransactionService transactionService;
  @Mock
  private TransactionAOMapper transactionAOMapper;
  @InjectMocks
  private TransactionApiService transactionApiService;

  private TransactionAO transactionAO = TransactionAO.builder()
      .transactionId(ID)
      .amount(MONEY)
      .timestamp(TIMESTAMP)
      .build();

  private Transaction transaction = Transaction.builder()
      .id(ID)
      .amount(MONEY)
      .timestamp(TIMESTAMP)
      .build();

  @Before
  public void setup() {
    transactionAO.add(new Link(HREF));
  }

  @Test
  public void findById() {
    when(transactionService.findById(ID)).thenReturn(Optional.of(transaction));
    when(transactionAOMapper.mapToApiObject(transaction)).thenReturn(transactionAO);

    assertThat(transactionApiService.findById(ID), is(Optional.of(transactionAO)));
  }

  @Test
  public void findById_Not_Found() {
    when(transactionService.findById(ID)).thenReturn(Optional.empty());

    assertThat(transactionApiService.findById(ID), is(Optional.empty()));

    verifyZeroInteractions(transactionAOMapper);
  }

  @Test
  public void findAll() {
    when(transactionService.findAll()).thenReturn(Collections.singletonList(transaction));
    when(transactionAOMapper.mapToApiObject(transaction)).thenReturn(transactionAO);

    assertThat(transactionApiService.findAll(), contains(transactionAO));
  }

  @Test
  public void findAll_Empty() {
    when(transactionService.findAll()).thenReturn(Collections.emptyList());

    assertThat(transactionApiService.findAll(), empty());

    verifyZeroInteractions(transactionAOMapper);
  }

  @Test
  public void create_Transaction() {
    setup_Create_Transaction_Test(TIMESTAMP.plusSeconds(30));

    assertThat(transactionApiService.create(transactionAO),
        is(new CreateTransactionResult(Status.TRANSACTION_OK, transactionAO)));
  }

  @Test
  public void create_Old_Transaction() {
    setup_Create_Transaction_Test(TIMESTAMP.plusSeconds(61));

    assertThat(transactionApiService.create(transactionAO),
        is(new CreateTransactionResult(Status.TRANSACTION_OLD, transactionAO)));
  }

  private void setup_Create_Transaction_Test(Instant clockNow) {
    when(transactionAOMapper.mapToDomainObject(transactionAO)).thenReturn(transaction);
    when(transactionService.save(transaction)).thenReturn(transaction);
    when(transactionAOMapper.mapToApiObject(transaction)).thenReturn(transactionAO);
    when(transactionClock.now()).thenReturn(clockNow);
  }
}