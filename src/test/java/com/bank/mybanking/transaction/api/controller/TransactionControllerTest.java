package com.bank.mybanking.transaction.api.controller;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.joda.money.CurrencyUnit.EUR;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bank.mybanking.transaction.api.domain.CreateTransactionResult;
import com.bank.mybanking.transaction.api.domain.CreateTransactionResult.Status;
import com.bank.mybanking.transaction.api.domain.TransactionAO;
import com.bank.mybanking.transaction.api.service.TransactionApiService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import java.util.Optional;
import lombok.SneakyThrows;
import org.joda.money.Money;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

  private static final String ID = "111";
  private static final Money MONEY = Money.of(EUR, 1.99d);
  private static final Instant NOW = Instant.now();
  private static final String HREF = "http://bank.com/transactions/" + ID;

  private TransactionAO transactionAO = TransactionAO.builder()
      .transactionId(ID)
      .amount(MONEY)
      .timestamp(NOW)
      .build();

  @Autowired
  private MockMvc mvc;
  @Autowired
  private ObjectMapper mapper;
  @MockBean
  private TransactionApiService transactionApiService;

  @Before
  public void setup() {
    transactionAO.add(new Link(HREF));
  }

  @Test
  @SneakyThrows
  public void getOne() {
    when(transactionApiService.findById(ID)).thenReturn(Optional.of(transactionAO));

    final String json = this.mvc
        .perform(get("/transactions/" + ID).accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();

    assertThat(mapper.readValue(json, TransactionAO.class), is(transactionAO));

    verify(transactionApiService).findById(ID);
  }

  @Test
  @SneakyThrows
  public void getOne_Empty() {
    when(transactionApiService.findById(ID)).thenReturn(Optional.empty());

    assertThat(this.mvc
        .perform(get("/transactions/" + ID).accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isNotFound())
        .andReturn().getResponse().getContentAsString(), isEmptyString());

    verify(transactionApiService).findById(ID);
  }

  @Test
  @SneakyThrows
  public void getAll() {
    when(transactionApiService.findAll()).thenReturn(singletonList(transactionAO));

    final String json = this.mvc
        .perform(get("/transactions").accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();

    assertThat(asList(mapper.readValue(json, TransactionAO[].class)), contains(transactionAO));

    verify(transactionApiService).findAll();
  }

  @Test
  @SneakyThrows
  public void getAll_Empty() {
    when(transactionApiService.findAll()).thenReturn(emptyList());

    final String json = this.mvc
        .perform(get("/transactions").accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andReturn().getResponse().getContentAsString();

    assertThat(asList(mapper.readValue(json, TransactionAO[].class)), empty());

    verify(transactionApiService).findAll();
  }

  @Test
  @SneakyThrows
  public void post_Transaction() {
    final CreateTransactionResult result =
        new CreateTransactionResult(Status.TRANSACTION_OK, transactionAO);

    when(transactionApiService.create(transactionAO)).thenReturn(result);

    this.mvc.perform(post("/transactions")
        .contentType(MediaType.APPLICATION_JSON)
        .content(mapper.writeValueAsString(transactionAO)))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(header().stringValues("Location", HREF));

    verify(transactionApiService).create(transactionAO);
  }

  @Test
  @SneakyThrows
  public void post_Old_Transaction() {
    final CreateTransactionResult result =
        new CreateTransactionResult(Status.TRANSACTION_OLD, transactionAO);

    when(transactionApiService.create(transactionAO)).thenReturn(result);

    transactionAO.setTimestamp(NOW.minusSeconds(61));

    this.mvc.perform(post("/transactions")
        .contentType(MediaType.APPLICATION_JSON)
        .content(mapper.writeValueAsString(transactionAO)))
        .andDo(print())
        .andExpect(status().isNoContent());

    verify(transactionApiService).create(transactionAO);
  }

}