package com.bank.mybanking.platform.mapping;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.time.Instant;

public class JavaInstantDeserializer extends StdDeserializer<Instant> {

  protected JavaInstantDeserializer() {
    super(Instant.class);
  }

  @Override
  public Instant deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    return Instant.ofEpochMilli(jp.readValueAs(Long.class));
  }
}