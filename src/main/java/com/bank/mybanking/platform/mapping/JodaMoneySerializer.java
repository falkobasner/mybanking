package com.bank.mybanking.platform.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

public class JodaMoneySerializer extends StdSerializer<Money> {

  protected JodaMoneySerializer() {
    super(Money.class);
  }

  @Override
  public void serialize(Money value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException {
    if (CurrencyUnit.EUR.equals(value.getCurrencyUnit())) {
      jgen.writeNumber(value.getAmount());
    } else {
      jgen.writeString(value.toString());
    }
  }
}