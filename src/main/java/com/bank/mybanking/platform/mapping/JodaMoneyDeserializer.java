package com.bank.mybanking.platform.mapping;

import static org.apache.commons.lang.math.NumberUtils.isNumber;
import static org.apache.commons.lang.math.NumberUtils.toDouble;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

public class JodaMoneyDeserializer extends StdDeserializer<Money> {

  protected JodaMoneyDeserializer() {
    super(Money.class);
  }

  @Override
  public Money deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    final String moneyStr = jp.readValueAs(String.class);

    if (isNumber(moneyStr)) {
      return Money.of(CurrencyUnit.EUR, toDouble(moneyStr));
    } else {
      return Money.parse(moneyStr);
    }
  }
}