package com.bank.mybanking.platform.mapping;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.time.Instant;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class JavaInstantModule extends SimpleModule {

  private static final String NAME = "InstantModule";
  private static final VersionUtil VERSION_UTIL = new VersionUtil() {
  };

  public JavaInstantModule() {
    super(NAME, VERSION_UTIL.version());
    addSerializer(Instant.class, new JavaInstantSerializer());
    addDeserializer(Instant.class, new JavaInstantDeserializer());
  }
}
