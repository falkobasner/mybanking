package com.bank.mybanking.platform.mapping;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.money.Money;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class JodaMoneyModule extends SimpleModule {

  private static final String NAME = "JodaMoneyModule";
  private static final VersionUtil VERSION_UTIL = new VersionUtil() {
  };

  public JodaMoneyModule() {
    super(NAME, VERSION_UTIL.version());
    addSerializer(Money.class, new JodaMoneySerializer());
    addDeserializer(Money.class, new JodaMoneyDeserializer());
  }
}
