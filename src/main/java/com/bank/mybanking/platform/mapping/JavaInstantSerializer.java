package com.bank.mybanking.platform.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.time.Instant;

public class JavaInstantSerializer extends StdSerializer<Instant> {

  protected JavaInstantSerializer() {
    super(Instant.class);
  }

  @Override
  public void serialize(Instant value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException {
    jgen.writeNumber(value.toEpochMilli());
  }
}