package com.bank.mybanking.platform.time;

import java.time.Instant;
import org.springframework.stereotype.Component;

@Component
public class TransactionClock {
  public Instant now() {
    return Instant.now();
  }
}
