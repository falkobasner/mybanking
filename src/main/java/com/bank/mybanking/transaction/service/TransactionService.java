package com.bank.mybanking.transaction.service;

import static com.bank.mybanking.transaction.service.TimestampIndexComparator.LOWER_BOUND_MIN_ID;
import static com.bank.mybanking.transaction.service.TimestampIndexComparator.UPPER_BOUND_MAX_ID;
import static java.util.Collections.synchronizedMap;
import static java.util.Collections.synchronizedSortedSet;
import static org.joda.money.CurrencyUnit.EUR;

import com.bank.mybanking.transaction.domain.Transaction;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import org.bson.types.ObjectId;
import org.joda.money.Money;
import org.springframework.stereotype.Component;

@Component
public class TransactionService {

  private Map<String, Transaction> data =
      synchronizedMap(new HashMap<>());

  private SortedSet<Transaction> timestampIndex =
      synchronizedSortedSet(new TreeSet<>(new TimestampIndexComparator()));

  public Optional<Transaction> findById(final String id) {
    return Optional.ofNullable(data.get(id));
  }

  public Collection<Transaction> findAll() {
    return data.values();
  }

  public Transaction save(final Transaction transaction) {
    if (transaction.getId() == null) {
      transaction.setId(new ObjectId().toString());
    }

    data.put(transaction.getId(), transaction);
    timestampIndex.add(transaction);

    return transaction;
  }

  public Collection<Transaction> findByTimestamp(final Instant from, final Instant to) {
    final Transaction lowerBound = new Transaction(LOWER_BOUND_MIN_ID, from, Money.zero(EUR));
    final Transaction upperBound = new Transaction(UPPER_BOUND_MAX_ID, to, Money.zero(EUR));

    return timestampIndex.subSet(lowerBound, upperBound);
  }

}
