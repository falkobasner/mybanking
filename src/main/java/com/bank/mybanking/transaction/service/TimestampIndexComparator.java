package com.bank.mybanking.transaction.service;

import com.bank.mybanking.transaction.domain.Transaction;
import java.util.Comparator;
import org.apache.commons.lang.ObjectUtils;

public class TimestampIndexComparator implements Comparator<Transaction> {

  public static final String LOWER_BOUND_MIN_ID = "MIN";
  public static final String UPPER_BOUND_MAX_ID = "MAX";

  @Override
  public int compare(Transaction t1, Transaction t2) {
    final int timestampResult = ObjectUtils.compare(t1.getTimestamp(), t2.getTimestamp());

    if (timestampResult != 0) {
      return timestampResult;
    }

    if (LOWER_BOUND_MIN_ID.equalsIgnoreCase(t1.getId())) {
      return -1;
    }

    if (UPPER_BOUND_MAX_ID.equalsIgnoreCase(t2.getId())) {
      return 1;
    }

    return ObjectUtils.compare(t1.getId(), t2.getId());
  }
}
