package com.bank.mybanking.transaction.api.service;

import static java.util.stream.Collectors.toList;

import com.bank.mybanking.platform.time.TransactionClock;
import com.bank.mybanking.transaction.api.domain.CreateTransactionResult;
import com.bank.mybanking.transaction.api.domain.CreateTransactionResult.Status;
import com.bank.mybanking.transaction.api.domain.TransactionAO;
import com.bank.mybanking.transaction.api.domain.TransactionAOMapper;
import com.bank.mybanking.transaction.service.TransactionService;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;

@Component
public class TransactionApiService {

  private final TransactionClock transactionClock;
  private final TransactionService transactionService;
  private final TransactionAOMapper transactionAOMapper;

  public TransactionApiService(
      final TransactionClock transactionClock,
      final TransactionService transactionService,
      final TransactionAOMapper transactionAOMapper) {
    this.transactionClock = transactionClock;
    this.transactionService = transactionService;
    this.transactionAOMapper = transactionAOMapper;
  }

  public Optional<TransactionAO> findById(final String id) {
    return transactionService.findById(id)
        .map(transactionAOMapper::mapToApiObject);
  }

  public List<TransactionAO> findAll() {
    return transactionService.findAll().stream()
        .map(transactionAOMapper::mapToApiObject)
        .collect(toList());
  }

  public CreateTransactionResult create(final TransactionAO postedObject) {
    final TransactionAO createdObject = transactionAOMapper.mapToApiObject(
        transactionService.save(transactionAOMapper.mapToDomainObject(postedObject)));

    if (createdObject.getTimestamp().isBefore(transactionClock.now().minusSeconds(60))) {
      return new CreateTransactionResult(Status.TRANSACTION_OLD, createdObject);
    } else {
      return new CreateTransactionResult(Status.TRANSACTION_OK, createdObject);
    }
  }
}
