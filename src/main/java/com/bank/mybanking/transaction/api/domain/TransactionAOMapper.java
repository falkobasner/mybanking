package com.bank.mybanking.transaction.api.domain;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.bank.mybanking.transaction.api.controller.TransactionController;
import com.bank.mybanking.transaction.domain.Transaction;
import org.springframework.stereotype.Component;

@Component
public class TransactionAOMapper {

  public TransactionAO mapToApiObject(final Transaction domainObject) {
    final TransactionAO apiObject = new TransactionAO();

    apiObject.setTransactionId(domainObject.getId());
    apiObject.setAmount(domainObject.getAmount());
    apiObject.setTimestamp(domainObject.getTimestamp());

    return enrichWithHateoas(apiObject);
  }

  public Transaction mapToDomainObject(final TransactionAO apiObject) {
    final Transaction domainObject = new Transaction();

    domainObject.setId(apiObject.getTransactionId());
    domainObject.setAmount(apiObject.getAmount());
    domainObject.setTimestamp(apiObject.getTimestamp());

    return domainObject;
  }

  private TransactionAO enrichWithHateoas(TransactionAO value) {
    value.add(linkTo(methodOn(TransactionController.class)
        .getOne(value.getTransactionId()))
        .withSelfRel());

    return value;
  }

}
