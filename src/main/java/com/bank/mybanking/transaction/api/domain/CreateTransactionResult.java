package com.bank.mybanking.transaction.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CreateTransactionResult {

  public static enum Status {
    TRANSACTION_OK, TRANSACTION_OLD
  }

  private Status status;

  private TransactionAO transaction;
}
