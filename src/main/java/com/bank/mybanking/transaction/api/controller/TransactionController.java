package com.bank.mybanking.transaction.api.controller;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

import com.bank.mybanking.transaction.api.domain.CreateTransactionResult;
import com.bank.mybanking.transaction.api.domain.CreateTransactionResult.Status;
import com.bank.mybanking.transaction.api.domain.TransactionAO;
import com.bank.mybanking.transaction.api.service.TransactionApiService;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

  private final TransactionApiService transactionApiService;

  public TransactionController(final TransactionApiService transactionApiService) {
    this.transactionApiService = transactionApiService;
  }

  @GetMapping("/{transactionId}")
  public ResponseEntity<TransactionAO> getOne(@PathVariable String transactionId) {
    return transactionApiService.findById(transactionId)
        .map(transaction -> new ResponseEntity<>(transaction, HttpStatus.OK))
        .orElse(new ResponseEntity<>(NOT_FOUND));
  }

  @GetMapping
  public ResponseEntity<List<TransactionAO>> getAll() {
    return ResponseEntity.ok(transactionApiService.findAll());
  }

  @PostMapping
  public ResponseEntity<Void> post(@RequestBody TransactionAO postedTransaction) {
    final CreateTransactionResult result = transactionApiService.create(postedTransaction);

    if (Status.TRANSACTION_OK.equals(result.getStatus())) {
      return created(
          fromUriString(result.getTransaction().getLink("self").getHref()).build().toUri()).build();
    } else {
      return noContent().build();
    }
  }
}
