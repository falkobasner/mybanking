package com.bank.mybanking.transaction.domain;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.money.Money;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Transaction {

  private String id;

  private Instant timestamp;

  private Money amount;
}
