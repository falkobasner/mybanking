package com.bank.mybanking.statistics.controller;

import com.bank.mybanking.statistics.domain.Statistics;
import com.bank.mybanking.statistics.service.StatisticsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

  private final StatisticsService statisticsService;

  public StatisticsController(final StatisticsService statisticsService) {
    this.statisticsService = statisticsService;
  }

  @GetMapping
  public ResponseEntity<Statistics> getStatistics() {
    return statisticsService.getTransactionStatistics()
        .map(ResponseEntity::ok)
        .orElse(ResponseEntity.notFound().build());
  }
}
