package com.bank.mybanking.statistics.service;

import com.bank.mybanking.platform.time.TransactionClock;
import com.bank.mybanking.statistics.domain.Statistics;
import com.bank.mybanking.transaction.domain.Transaction;
import com.bank.mybanking.transaction.service.TransactionService;
import java.time.Instant;
import java.util.Collection;
import java.util.Optional;
import org.springframework.stereotype.Component;

@Component
public class StatisticsService {

  private final TransactionClock transactionClock;
  private final TransactionService transactionService;

  public StatisticsService(
      final TransactionClock transactionClock,
      final TransactionService transactionService) {
    this.transactionClock = transactionClock;
    this.transactionService = transactionService;
  }

  public Optional<Statistics> getTransactionStatistics() {
    final Instant now = transactionClock.now();

    final Collection<Transaction> transactions = transactionService
        .findByTimestamp(now.minusSeconds(60), now);

    if (transactions.isEmpty()) {
      return Optional.empty();
    }

    return Optional.of(new Statistics(
        getSum(transactions),
        getAvg(transactions),
        getMax(transactions),
        getMin(transactions),
        new Integer(transactions.size()).longValue()));
  }

  double getSum(final Collection<Transaction> transactions) {
    return transactions.stream()
        .mapToDouble(t -> t.getAmount().getAmount().doubleValue())
        .sum();
  }

  double getAvg(final Collection<Transaction> transactions) {
    return transactions.stream()
        .mapToDouble(t -> t.getAmount().getAmount().doubleValue())
        .average()
        .orElse(0);
  }

  double getMax(final Collection<Transaction> transactions) {
    return transactions.stream()
        .mapToDouble(t -> t.getAmount().getAmount().doubleValue())
        .max()
        .orElse(0);
  }

  double getMin(final Collection<Transaction> transactions) {
    return transactions.stream()
        .mapToDouble(t -> t.getAmount().getAmount().doubleValue())
        .min()
        .orElse(0);
  }
}
