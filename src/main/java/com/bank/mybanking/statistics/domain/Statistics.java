package com.bank.mybanking.statistics.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Statistics {

  private Double sum;
  private Double avg;
  private Double max;
  private Double min;
  private Long count;
}
